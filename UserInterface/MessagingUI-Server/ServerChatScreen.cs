﻿//-----------------------------------------------------------------------
// <author>
//      Jayaprakash A
// </author>
// <reviewer>
//      Polu Varshith
// </reviewer>
// <date>
//      07-Nov-2018
// </date>
// <summary>
//      Functionalities of UI design
// </summary>
// <copyright file="ServerChatScreen.cs" company="B'15, IIT Palakkad">
//     This project is licensed under GNU General Public License v3. (https://fsf.org) 
// </copyright>
//-----------------------------------------------------------------------

namespace MASTI.MessagingUIServer
{
    using System;
    using System.Drawing;
    using System.IO;
    using System.Windows.Forms;
    using System.Resources;
    using System.Net.Sockets;
    using System.Net;
    using Masti.ImageProcessing;
    using Messenger;
    /// <summary>
    /// Defines the <see cref="ServerChatScreen" />
    /// </summary>
    public partial class ServerChatScreen : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ServerChatScreen"/> class.
        /// </summary>
        /// 

        public IUXMessage messageHandler = null;
        public IImageProcessing imageHandler = null;
        //public string fromIp = GetLocalIPAddress();
        public IPAddress toIP = null;
        public IPAddress fromIP = IPAddress.Parse(GetLocalIPAddress());
        public int toPort = 0;

        public ServerChatScreen()
        {
            this.InitializeComponent();
            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Path = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            watcher.NotifyFilter = NotifyFilters.LastWrite;
            watcher.Filter = "trigger.txt";
            watcher.Changed += new FileSystemEventHandler(this.OnMessageReceived);
            watcher.EnableRaisingEvents = true;
        }

        /// <summary>
        /// The InvokeMessageDelegate
        /// </summary>
        /// <param name="clientName">The clientName<see cref="string"/></param>
        /// <param name="message">The message<see cref="string"/></param>
        /// <param name="isClientMessage">The isClientMessage<see cref="string"/></param>
        internal delegate void InvokeMessageDelegate(string clientName, string message, string isClientMessage, bool success);

        /// <summary>
        /// The OnMessageReceived
        /// </summary>
        /// <param name="source">The source<see cref="object"/></param>
        /// <param name="e">The e<see cref="FileSystemEventArgs"/></param>
        internal void OnMessageReceived(object source, FileSystemEventArgs e)
        {
            string path = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\" + "messages.txt";
            try
            {
                string[] lines = File.ReadAllLines(path);
                if (lines.Length > 1)
                {
                    this.BeginInvoke(new InvokeMessageDelegate(this.InvokeMessage), lines[0], lines[1], lines[2], true);
                }
            }
            catch (Exception)
            {
                // Handle the exception
            }
        }

        /// <summary>
        /// The InvokeMessage
        /// </summary>
        /// <param name="clientName">The clientName<see cref="string"/></param>
        /// <param name="message">The message<see cref="string"/></param>
        /// <param name="isClientMessage">The isClientMessage<see cref="string"/></param>
        public void InvokeMessage(string clientName, string message, string isClientMessage, bool success)
        {
            //Console.WriteLine("Invoke message");
            TabPage messagePage = null;
            TextBox unreadMessageCountTextBox = null;
            TextBox tabPageTop = null;
            TabControl.TabPageCollection pages = ServerChatSectionTabs.TabPages;
            SplitContainer splitContainer = null;
            foreach (TabPage page in pages)
            {
                if (page.Name.Equals(clientName + "tabPage"))
                {
                    messagePage = page;

                    Control.ControlCollection controls1 = messagePage.Controls;
                    foreach (Control control in controls1)
                    {
                        if (control.Name.Equals(clientName + "split"))
                        {
                            splitContainer = control as SplitContainer;
                        }
                    }
                    break;
                }
            }
            //Console.WriteLine("Page not found out");
            int tabPageNextTopHeight = 0;

            if (messagePage == null)
            {
                messagePage = CreateMessagePage(clientName);
                Console.WriteLine(messagePage);

                splitContainer = new SplitContainer();
                splitContainer.Dock = DockStyle.Fill;
                splitContainer.Name = clientName + "split";
                splitContainer.Orientation = Orientation.Horizontal;
                
                //Console.WriteLine("vertical scroll bar" + SystemInformation.VerticalScrollBarWidth);
                //splitContainer.Panel2.Padding = new Padding(50, 0, 50, 0);

                SplitterPanel topPanel = splitContainer.Panel1;
                SplitterPanel bottomPanel = splitContainer.Panel2;

                float h = (float)this.ServerChatSectionTabs.Height;
                h *= (float)0.75;
                int g = (int)Math.Ceiling(h);
                splitContainer.SplitterDistance = g;
                splitContainer.Panel1Collapsed = true;
                splitContainer.Panel2.AutoScroll = true;
                splitContainer.Panel2.AutoScrollMinSize = new Size(10, 0);
                //splitContainer.Panel2.SetAutoScrollMargin(SystemInformation.VerticalScrollBarWidth, 0);
                splitContainer.Panel2.Padding = new Padding(0, 0, SystemInformation.VerticalScrollBarWidth, 0);
                TextBox timeStamp = new TextBox();
                timeStamp.Name = clientName + "timeStamp";
                timeStamp.Text = clientName;
                timeStamp.BorderStyle = BorderStyle.None;
                timeStamp.Height = 100;
                timeStamp.WordWrap = true;
                timeStamp.Visible = false;
                timeStamp.BackColor = Color.Black;
                timeStamp.ForeColor = Color.White;
                timeStamp.TextAlign = HorizontalAlignment.Center;

                PictureBox screenShareImageBox = new PictureBox();
                screenShareImageBox.Name = clientName + "pictureBox";
                screenShareImageBox.Text = clientName + "pictureBox";
                screenShareImageBox.Height = g;
                screenShareImageBox.SizeMode = PictureBoxSizeMode.StretchImage;
                screenShareImageBox.Width = this.ServerChatSectionTabs.Width;
                screenShareImageBox.Visible = false;

                topPanel.Controls.Add(timeStamp);
                topPanel.Controls.Add(screenShareImageBox);
                messagePage.Controls.Add(splitContainer);
                ServerChatSectionTabs.TabPages.Add(messagePage);
                pages = ServerChatSectionTabs.TabPages;
                foreach (TabPage page in pages)
                {

                    Console.WriteLine(page.Name + " " + messagePage.Name);
                }
                //messagePage.Controls.Add(timeStamp);
                //messagePage.Controls.Add(screenShareImageBox);                
            }
            Control.ControlCollection controls = messagePage.Controls;
            foreach (Control control in controls)
            {
                if (control.Name.Equals(clientName + "unreadMessage"))
                {
                    unreadMessageCountTextBox = control as TextBox;
                }
                if (control.Name.Equals(clientName + "tabPageTop"))
                {
                    tabPageTop = control as TextBox;
                    tabPageNextTopHeight = int.Parse(tabPageTop.Text);
                }
            }

            TabPage currentPage = ServerChatSectionTabs.SelectedTab;

            try
            {
                ServerChatSectionTabs.TabPages.Remove(messagePage);
            }
            catch (Exception e)
            {
                //pages = ServerChatSectionTabs.TabPages;

                Console.WriteLine("An error occurred: '{0}'", e);
            }

            ServerChatSectionTabs.TabPages.Insert(0, messagePage);
            /*pages = ServerChatSectionTabs.TabPages;
            foreach (TabPage page in pages)
            {
                Console.WriteLine(page.Name + " " + messagePage.Name);
            }*/
            ServerChatSectionTabs.SelectTab(ServerChatSectionTabs.TabPages.IndexOf(currentPage));

            //Console.WriteLine("Tab page insertion deletion select tab successful");
            string unreadMessageCount = unreadMessageCountTextBox.Text;
            string currentClientName = messagePage.Text;
            int count = int.Parse(unreadMessageCount);
            if (isClientMessage.Equals("Client"))
            {
                count += 1;
            }
            string strOfCount = count.ToString();
            string updatedCount = "";

            if (strOfCount.Length == 1)
            {
                updatedCount = "0" + strOfCount;
            }
            else if (strOfCount.Length == 2)
            {
                updatedCount = strOfCount;
            }
            else
            {
                updatedCount = "99";
            }
            //Console.WriteLine("Count update successful");
            unreadMessageCountTextBox.Text = updatedCount;
            string tabPageNewHeight = createMessageTextBox(isClientMessage, tabPageNextTopHeight, messagePage, message, splitContainer, success);
            tabPageTop.Text = tabPageNewHeight;
        }

        /// <summary>
        /// The createMessageTextBox
        /// </summary>
        /// <param name="isClientMessage">The isClientMessage<see cref="string"/></param>
        /// <param name="tabPageNextTopHeight">The tabPageNextTopHeight<see cref="int"/></param>
        /// <param name="messagePage">The messagePage<see cref="TabPage"/></param>
        /// <param name="message">The message<see cref="string"/></param>
        /// <returns>The <see cref="string"/></returns>
        private string createMessageTextBox(string isClientMessage, int tabPageNextTopHeight, TabPage messagePage, string message, SplitContainer s, bool success)
        {
            RichTextBox messageBox = new RichTextBox();

            messageBox.Font = new Font("Lucinda Console", 12);
            float adjustedWidth = (float)0.7 * s.Panel2.Width;
            float shiftedWidth = s.Panel2.Width - (float)0.7 * s.Panel2.Width;// - SystemInformation.VerticalScrollBarWidth;
            messageBox.Width = (int)adjustedWidth;
            messageBox.Height = 30;
            messageBox.ReadOnly = true;
            messageBox.ContentsResized += new ContentsResizedEventHandler(TextBoxContentResized);
            Console.WriteLine("After change new message" + (int)shiftedWidth);
            int prevVerticalLocation = s.Panel2.VerticalScroll.Value;
            int prevHorizontalLocation = s.Panel2.HorizontalScroll.Value;
            s.Panel2.VerticalScroll.Value = 0;
            s.Panel2.HorizontalScroll.Value = 0;
            Console.WriteLine("Previous Scroll Locations " + prevHorizontalLocation + " " + prevVerticalLocation);
            s.Panel2.SizeChanged += new System.EventHandler(panelSizeChanged);

            if (isClientMessage.Equals("Server") && success)
            {
                messageBox.Location = new Point((int)shiftedWidth, tabPageNextTopHeight + 5);
               // messageBox.Dock = DockStyle.Right;

               // messageBox.Anchor = AnchorStyles.Top | AnchorStyles.Right;
                messageBox.ForeColor = Color.Blue;
            }
            else if (isClientMessage.Equals("Client"))
            {
                //messageBox.Dock = DockStyle.Left;
               // messageBox.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                messageBox.Location = new Point(0, tabPageNextTopHeight + 5);
                messageBox.ForeColor = Color.Orange;
            }
            else if (isClientMessage.Equals("Server") && !success)
            {
               // messageBox.Dock = DockStyle.Right;
                //messageBox.Anchor = AnchorStyles.Top | AnchorStyles.Right;
                messageBox.Location = new Point((int)shiftedWidth, tabPageNextTopHeight + 5);
                messageBox.ForeColor = Color.Red;
            }
            messageBox.AppendText(message);
            messageBox.AppendText(System.Environment.NewLine);
            messageBox.Select(0, message.Length);
            messageBox.SelectionAlignment = HorizontalAlignment.Left;
            int prevLength = messageBox.TextLength;
            string time = DateTime.Now.ToString("HH:mm tt");
            Console.WriteLine("Time " + time);
            DateTime dt = DateTime.Parse(time); // No error checking
            time = dt.ToString("HH:mm");
            string date = DateTime.Now.ToString("MM/dd");
            time = date + " " + time;
            if (success)
            {
                messageBox.AppendText(time);
                messageBox.Select(prevLength, time.Length);
                Console.WriteLine("Selected Text" + messageBox.SelectedText);
            }
            else
            {
                string errMessage = "Message not delivered";
                messageBox.AppendText(errMessage);
                messageBox.Select(prevLength, errMessage.Length);
                Console.WriteLine("Selected Text" + messageBox.SelectedText);
            }
            messageBox.SelectionFont = new Font("Lucinda Console", 8);
            messageBox.SelectionColor = Color.Black;
            messageBox.SelectionAlignment = HorizontalAlignment.Right;

            s.Panel2.Controls.Add(messageBox);

            messagePage.Controls.Add(s);
            tabPageNextTopHeight += (messageBox.Height + 5);

            //s.Panel2.ScrollControlIntoView(messageBox);

            Console.WriteLine("Locations After New MessageBox " + s.Panel2.VerticalScroll.Value + " " + s.Panel2.HorizontalScroll.Value);
            s.Panel2.VerticalScroll.Value = prevVerticalLocation;
            Console.WriteLine("Setting horizontal scroll" + prevHorizontalLocation);
            s.Panel2.HorizontalScroll.Value = prevHorizontalLocation;
            //Console.WriteLine("scroll " + prevHorizontalLocation + " " + prevVerticalLocation);
            Console.WriteLine("Locations After New MessageBox(Updated) " + s.Panel2.VerticalScroll.Value + " " + s.Panel2.HorizontalScroll.Value);
            return tabPageNextTopHeight.ToString();
        }

        private void panelSizeChanged(object sender, EventArgs e)
        {
            TabPage activeTab = ServerChatSectionTabs.SelectedTab;
            Control [] actveTabControls = activeTab.Controls.Find(activeTab.Text + "split", true);
            SplitContainer s = actveTabControls[0] as SplitContainer;
            foreach (Control c in s.Controls)
            {
                float adjustedWidth = (float)0.7 * s.Panel2.Width;
                float shiftedWidth = s.Panel2.Width - (float)0.7 * s.Panel2.Width;// - SystemInformation.VerticalScrollBarWidth;
                foreach (Control rtb in c.Controls)
                {

                    if (rtb is RichTextBox)
                    {
                        if (rtb.Location.X != 0)
                        {
                            rtb.Location = new Point((int)shiftedWidth, rtb.Location.Y);
                        }
                        rtb.Width = (int)adjustedWidth;
                    }

                }
            }
        }

        /// <summary>
        /// The CreateMessagePage
        /// </summary>
        /// <param name="clientName">The clientName<see cref="string"/></param>
        /// <returns>The <see cref="TabPage"/></returns>
        private TabPage CreateMessagePage(string clientName)
        {
            TabPage messagePage = new TabPage();
            messagePage.Name = clientName + "tabPage";
            messagePage.Text = clientName;
            messagePage.AutoScroll = true;
            messagePage.HorizontalScroll.Enabled = false;
            messagePage.VerticalScroll.Enabled = true;
            messagePage.Size = new Size(ServerChatSectionTabs.Width, ServerChatSectionTabs.Height);

            TextBox unreadMessageCountTextBox = new TextBox();
            unreadMessageCountTextBox.Name = clientName + "unreadMessage";
            unreadMessageCountTextBox.Height = this.ServerChatSectionTabs.Height;
            unreadMessageCountTextBox.Width = this.ServerChatSectionTabs.Width;
            unreadMessageCountTextBox.Visible = false;
            unreadMessageCountTextBox.Text = "00";

            TextBox tabPageTop = new TextBox();
            tabPageTop.Height = 100;
            tabPageTop.Width = 50;
            tabPageTop.Name = clientName + "tabPageTop";
            tabPageTop.Text = "0";
            tabPageTop.Visible = false;

            messagePage.Controls.Add(unreadMessageCountTextBox);
            messagePage.Controls.Add(tabPageTop);

            return messagePage;
        }

        /// <summary>
        /// The TextBoxContentResized
        /// </summary>
        /// <param name="Sender">The Sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="ContentsResizedEventArgs"/></param>
        private void TextBoxContentResized(object Sender, ContentsResizedEventArgs e)
        {
            ((RichTextBox)Sender).Height = e.NewRectangle.Height + 5;
        }

        /// <summary>
        /// The AlignAndPaintTabs
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="DrawItemEventArgs"/></param>
        private void AlignAndPaintTabs(object sender, DrawItemEventArgs e)
        {
            //Console.WriteLine("Started");
            Graphics g = e.Graphics;
            Brush textBrush;

            if (e.Index >= ServerChatSectionTabs.TabPages.Count)
            {
                return;
            }

            TabPage tabPage = ServerChatSectionTabs.TabPages[e.Index];
            tabPage.BorderStyle = BorderStyle.None;
            Rectangle tabBounds = ServerChatSectionTabs.GetTabRect(e.Index);
            Rectangle unreadMessageCountBounds = new Rectangle(180, tabBounds.Y + 28, 30, 30);
            Brush gray = new SolidBrush(System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(245))))));
            Brush graySelected = new SolidBrush(System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225))))));

            if (e.State == DrawItemState.Selected)
            {
                g.FillRectangle(graySelected, e.Bounds);
            }
            else
            {
                g.FillRectangle(gray, e.Bounds);
            }
            //Console.WriteLine("Brushes and bounds fixed");
            Color violet = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(29)))), ((int)(((byte)(58)))));
            textBrush = new SolidBrush(violet);
            Font tabFontActive = new Font("Segoe UI", (float)15.0, FontStyle.Bold, GraphicsUnit.Pixel);
            Font tabFont = new Font("Segoe UI", (float)15.0, FontStyle.Regular, GraphicsUnit.Pixel);
            Font unreadMessageFont = new Font("Segoe UI", (float)12.0, FontStyle.Bold, GraphicsUnit.Pixel);

            StringFormat stringFlags = new StringFormat();
            stringFlags.Alignment = StringAlignment.Center;
            stringFlags.LineAlignment = StringAlignment.Center;

            string tabPageName = tabPage.Text;
            Control[] list = tabPage.Controls.Find(tabPageName + "unreadMessage", true);
            string unreadMessageCount = list[0].Text;
            //Console.WriteLine("Appropriate strings found out");
            if (e.State == DrawItemState.Selected)
            {
                list[0].Text = "00";
                g.DrawString(tabPageName, tabFontActive, textBrush, tabBounds, new StringFormat(stringFlags));
                g.DrawString("00", unreadMessageFont, textBrush, unreadMessageCountBounds, new StringFormat(stringFlags));
            }
            else
            {
                g.DrawString(tabPageName, tabFont, textBrush, tabBounds, new StringFormat(stringFlags));
                g.DrawString(unreadMessageCount, unreadMessageFont, textBrush, unreadMessageCountBounds, new StringFormat(stringFlags));
            }
        }

        /// <summary>
        /// The PortNumberCharacterEntry
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="KeyPressEventArgs"/></param>
        private void PortNumberCharacterEntry(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        /// <summary>
        /// The SelectedIndexChanged
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="EventArgs"/></param>
        private void SelectedIndexChanged(object sender, EventArgs e)
        {
            TabPage tabPage = ServerChatSectionTabs.SelectedTab;
            string tabPageName = tabPage.Text;
            Control[] actveTabControls = tabPage.Controls.Find(tabPage.Text + "split", true);
            SplitContainer s = actveTabControls[0] as SplitContainer;
            foreach (Control c in s.Controls)
            {
                float adjustedWidth = (float)0.7 * s.Panel2.Width;
                float shiftedWidth = s.Panel2.Width - (float)0.7 * s.Panel2.Width;// - SystemInformation.VerticalScrollBarWidth;
                foreach (Control rtb in c.Controls)
                {

                    if (rtb is RichTextBox)
                    {
                        if (rtb.Location.X != 0)
                        {
                            rtb.Location = new Point((int)shiftedWidth, rtb.Location.Y);
                        }
                        rtb.Location = new Point((int)shiftedWidth, rtb.Location.Y);
                        rtb.Width = (int)adjustedWidth;
                    }

                }
            }
            Control[] list = tabPage.Controls.Find(tabPageName + "unreadMessage", true);
            string unreadMessageCount = "00";
            list[0].Text = unreadMessageCount;

            SplitContainer splitContainer = null;
            Control.ControlCollection controls = tabPage.Controls;
            foreach (Control control in controls)
            {
                if (control.Name.Equals(tabPageName + "split"))
                {
                    splitContainer = control as SplitContainer;
                    Console.WriteLine("Split :" + splitContainer);
                }
            }
            if (splitContainer.Panel1Collapsed)
            {
                shareScreenButton.Image = Image.FromFile("..\\..\\Images\\startScreen.png");
            }
            else
            {
                shareScreenButton.Image = Image.FromFile("..\\..\\Images\\stopScreen.png");
            }
        }

        /// <summary>
        /// The PortButtonClick
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="EventArgs"/></param>
        private void PortButtonClick(object sender, EventArgs e)
        {
            //do something
            bool isPortNumberValid = true;
            if(portTextBox.TextLength == 0)
            {
                MessageBox.Show("Please enter a port number");
                return;
            }
            if (isPortNumberValid)
            {
                shareScreenButton.Enabled = true;
                sendButton.Enabled = true;
                serverMessageTextBox.Enabled = true;
            }
            else
            {
                MessageBox.Show("Try a different port");
            }
            toPort = int.Parse(portTextBox.Text);
            messageHandler = new Messager(toPort);
            imageHandler = new ImageProcessing();

            messageHandler.SubscribeToDataReceiver(ReceiverMessageHandler);
            messageHandler.SubscribeToStatusReceiver(sendingStatusHandlers);
        }

        public void ReceiverMessageHandler(string message, string toIP, string fromIP, DateTime dateTime)
        {
            InvokeMessage(fromIP, message,"Client", true);
        }
        public void sendingStatusHandlers(Messenger.Handler.StatusCode status, string message)
        {
            if (status.ToString() == "Success")
            {
                string clientName = ServerChatSectionTabs.SelectedTab.Text;
                InvokeMessage(clientName, message, "Server", true);
                serverMessageTextBox.Text = "";
            }
            else
            {
                string clientName = ServerChatSectionTabs.SelectedTab.Text;
                InvokeMessage(clientName, message, "Server", false);
                serverMessageTextBox.Text = "";
            }
        }

        private void sendMessage(object sender, EventArgs e)
        {
            //bool messageSent = false;
            //Do something

            /*if (messageSent)
            {
                string clientName = ServerChatSectionTabs.SelectedTab.Text;
                InvokeMessage(clientName, serverMessageTextBox.Text, "Server", true);
                serverMessageTextBox.Text = "";
            }
            else
            {
                string clientName = ServerChatSectionTabs.SelectedTab.Text;
                InvokeMessage(clientName, serverMessageTextBox.Text, "Server", false);
                serverMessageTextBox.Text = "";
            }*/
            string clientName = ServerChatSectionTabs.SelectedTab.Text;
            messageHandler.SendMessage(serverMessageTextBox.Text,fromIP,,1000,);
        }

        public static Bitmap GetImageByName(string imageName)
        {
            System.Reflection.Assembly asm = System.Reflection.Assembly.GetExecutingAssembly();
            string resourceName = asm.GetName().Name + ".Properties.Resources";
            var rm = new System.Resources.ResourceManager(resourceName, asm);
            return (Bitmap)rm.GetObject(imageName);

        }

        private void shareScreenButton_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Initial Page width" + ServerChatSectionTabs.SelectedTab.Width + " " + this.Width);

            string currentTabName = ServerChatSectionTabs.SelectedTab.Name;
            string clientName = currentTabName.Substring(0, currentTabName.Length - 7);
            string currentMessagePage = currentTabName.Substring(0, currentTabName.Length - 7) + "tabPage";
            string currentTimeStamp = currentTabName.Substring(0, currentTabName.Length - 7) + "timeStamp";
            string currentScreenShareImageBox = currentTabName.Substring(0, currentTabName.Length - 7) + "pictureBox";
            string currentSplit = currentTabName.Substring(0, currentTabName.Length - 7) + "split";

            TabControl.TabPageCollection pages = ServerChatSectionTabs.TabPages;
            TabPage messagePage = null;
            PictureBox currentImageBox = null;
            TextBox currentTime = null;
            TextBox currentMessage = null;
            SplitContainer splitContainer = null;
            foreach (TabPage page in pages)
            {
                if (page.Name.Equals(currentMessagePage))
                {
                    messagePage = page;
                    break;
                }
            }

            Control.ControlCollection controls = messagePage.Controls;
            foreach (Control control in controls)
            {
                if (control.Name.Equals(currentSplit))
                {
                    splitContainer = control as SplitContainer;
                    Console.WriteLine("Split :" + splitContainer);
                }
            }

            Control.ControlCollection control12 = splitContainer.Panel1.Controls;
            foreach (Control control in control12)
            {
                if (control.Name.Equals(currentScreenShareImageBox))
                {
                    currentImageBox = control as PictureBox;
                    Console.WriteLine("PictureBox :" + currentImageBox);
                }

                if (control.Name.Equals(currentTimeStamp))
                {
                    currentTime = control as TextBox;
                    Console.WriteLine("TimeStamp :" + currentTime);
                }
            }


            DateTime CurrentDate;
            CurrentDate = Convert.ToDateTime(DateTime.Now.ToString("dd-MMM-yyyy"));
            //currentTime.Text = CurrentDate.ToString;

            bool isFront = currentImageBox.Visible;

            Console.WriteLine("Isfront value:" + isFront);
            if (!isFront)
            {
                splitContainer.Panel1Collapsed = false;
                shareScreenButton.Image = Image.FromFile("..\\..\\Images\\stopScreen.png");
                currentImageBox.ImageLocation = "..\\..\\Images\\client1.bmp";
                currentTime.Visible = true;
                currentImageBox.Visible = true;
            }
            else
            {
                shareScreenButton.Image = Image.FromFile("..\\..\\Images\\startScreen.png");
                currentTime.Visible = false;
                currentImageBox.Visible = false;
                splitContainer.Panel1Collapsed = true;
            }


        }

        private void ServerChatScreen_Load(object sender, EventArgs e)
        {
            shareScreenButton.Image = Image.FromFile("..\\..\\Images\\startScreen.png");
        }


        private static string GetLocalIPAddress()
        {
            if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                MessageBox.Show("No network connection available");
                return "";
            }
            var host = Dns.GetHostEntry(Dns.GetHostName());
            try
            {
                foreach (var ip in host.AddressList)
                {
                    if (ip.AddressFamily == AddressFamily.InterNetwork)
                    {
                        return ip.ToString();
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            return "";
        }

    }
}
