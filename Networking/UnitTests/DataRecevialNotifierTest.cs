﻿// -----------------------------------------------------------------------
// <author> 
//      Libin N George
// </author>
//
// <date> 
//      03-11-2018 
// </date>
// 
// <reviewer>
//      Ayush Mittal
// </reviewer>
//
// <copyright file="DataRecevialNotifierTest.cs" company="B'15, IIT Palakkad">
//      This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
//
// <summary>
//      This file contains Tests for Data Receiver Notifier Component 
//      This file is a part of Networking Module Unit Testing
// </summary>
// -----------------------------------------------------------------------

namespace Masti.Networking.UnitTests
{
    using System;
    using System.Globalization;
    using System.Net;
    using Masti.QualityAssurance;

    /// <summary>
    /// Unit Test for Data on Receive Notifier Component.
    /// </summary>
    public class DataRecevialNotifierTest : ITest
    {        
        /// <summary>
        /// status to check that delegate is called while receiving Image related Data.  
        /// </summary>
        private bool imagedataRecevied;

        /// <summary>
        /// status to check that delegate is called while receiving Messaging related Data. 
        /// </summary>
        private bool messagedataRecevied;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataRecevialNotifierTest" /> class.
        /// </summary>
        /// <param name="logger">Logger object used for logging during test</param>
        public DataRecevialNotifierTest(ILogger logger)
        {
            this.Logger = logger;
        }

        /// <summary>
        /// Gets or sets Logger instance.
        /// </summary>
        public ILogger Logger { get; set; }

        /// <summary>
        /// Function for running the unit tests
        /// </summary>
        /// <returns>true on success and false on failure</returns>
        public bool Run()
        {
            // boolean capturing the status of subscription for ImageProcessing Module
            bool imageModuleSubscription = false;

            // boolean capturing the status of subscription for Messaging Module
            bool messageModuleSubscription = false;

            // boolean capturing the status of successful type recognition and calling delegate for ImageProcessing Module Data packet
            bool imageDataReceiveNotified = false;

            // boolean capturing the status of successful type recognition and calling delegate for Messageing Module Data packet
            bool messageDataReceiveNotified = false;

            // boolean capturing the status of successful type recognition for Unknown Module Data packet
            bool invalidDataReceiveNotified;

            this.imagedataRecevied = false;
            this.messagedataRecevied = false;
            using (Communication communication = new Communication(4000, 3, 1))
            {
                // Setting Stub Schema for this test.
                communication.Schema = new StubSchemaForNetworking();
                imageModuleSubscription = this.SubscribeForImageDataTest(communication);
                messageModuleSubscription = this.SubscribeForMessageDataTest(communication);

                if (!(imageModuleSubscription & messageModuleSubscription))
                {
                    return false;
                }

                invalidDataReceiveNotified = communication.DataReceiveNotifier("UNKNOWN Packet", IPAddress.Parse("127.0.0.2"));
                if (!(invalidDataReceiveNotified | this.imagedataRecevied | this.messagedataRecevied))
                {
                    this.Logger.LogSuccess(string.Format(CultureInfo.CurrentCulture, "Invalid type successfully detected"));
                }
                else
                {
                    this.Logger.LogError(string.Format(CultureInfo.CurrentCulture, "Invalid type not detected"));
                    return false;
                }

                imageDataReceiveNotified = communication.DataReceiveNotifier("ImageProcessing Packet", IPAddress.Parse("127.0.0.1"));
                if (!imageDataReceiveNotified)
                {
                    this.Logger.LogError(string.Format(CultureInfo.CurrentCulture, "Notification for Image packet failed due unknown type"));
                    return false;
                }

                messageDataReceiveNotified = communication.DataReceiveNotifier("Messaging Packet", IPAddress.Parse("127.0.0.2"));
                if (!messageDataReceiveNotified)
                {
                    this.Logger.LogError(string.Format(CultureInfo.CurrentCulture, "Notification for Message packet failed due unknown type"));
                    return false;
                }

                return this.imagedataRecevied & this.messagedataRecevied;
            }
        }

        /// <summary>
        /// Function used as event handler for receiving Image related package.
        /// </summary>
        /// <param name="data">Image related data received</param>
        /// <param name="ipAddress">IP address of machine from which data packet is received</param>
        private void ReceiveImageData(string data, IPAddress ipAddress)
        {
            if (ipAddress == null)
            {
                this.Logger.LogError(string.Format(CultureInfo.CurrentCulture, "Invalid IP Address : IP Address is null."));
                return;
            }

            this.Logger.LogSuccess(string.Format(CultureInfo.CurrentCulture, "Data Received {0} from IP Address {1} For Image Processing Module", data, ipAddress.ToString()));
            this.imagedataRecevied = true;
        }

        /// <summary>
        /// Function used as event handler for receiving Message related package.
        /// </summary>
        /// <param name="data">Message related data received</param>
        /// <param name="ipAddress">IP Address of machine from which data packet is received</param>
        private void ReceiveMessageData(string data, IPAddress ipAddress)
        {
            if (ipAddress == null)
            {
                this.Logger.LogError(string.Format(CultureInfo.CurrentCulture, "Invalid IP Address : IP Address is null."));
                return;
            }

            this.Logger.LogSuccess(string.Format(CultureInfo.CurrentCulture, "Data Received {0} from IP Address {1} For Messaging Module", data, ipAddress.ToString()));
            this.messagedataRecevied = true;
        }

        /// <summary>
        /// Tests the subscription for Image Processing Module
        /// </summary>
        /// <param name="communication">communication Instance</param>
        /// <returns>true on success</returns>
        private bool SubscribeForImageDataTest(Communication communication)
        {
            if (communication == null)
            {
                this.Logger.LogError(string.Format(CultureInfo.CurrentCulture, "Invalid Communication Object : communication variable is null."));
                return false;
            }

            bool subscribeStatus = false;
            subscribeStatus = communication.SubscribeForDataReceival(DataType.ImageSharing, this.ReceiveImageData);
            if (subscribeStatus)
            {
                this.Logger.LogSuccess(string.Format(CultureInfo.CurrentCulture, "Subscription for Receiving Data for {0} completed successfully.", DataType.ImageSharing.ToString()));
            }
            else
            {
                this.Logger.LogError(string.Format(CultureInfo.CurrentCulture, "Subscription for Receiving Data for {0} failed." + DataType.ImageSharing.ToString()));
            }

            return subscribeStatus;
        }

        /// <summary>
        /// Tests the subscription for Messaging Module
        /// </summary>
        /// <param name="communication">communication Instance</param>
        /// <returns>true on success</returns>
        private bool SubscribeForMessageDataTest(Communication communication)
        {
            if (communication == null)
            {
                this.Logger.LogError(string.Format(CultureInfo.CurrentCulture, "Invalid Communication Object : communication variable is null."));
                return false;
            }

            bool subscribeStatus = false;
            subscribeStatus = communication.SubscribeForDataReceival(DataType.Message, this.ReceiveMessageData);
            if (subscribeStatus)
            {
                this.Logger.LogSuccess(string.Format(CultureInfo.CurrentCulture, "Subscription for Receiving Data for {0} completed successfully.", DataType.Message.ToString()));
            }
            else
            {
                this.Logger.LogError(string.Format(CultureInfo.CurrentCulture, "Subscription for Receiving Data for {0} failed." + DataType.Message.ToString()));
            }

            return subscribeStatus;
        }
    }
}
