﻿// -----------------------------------------------------------------------
// <author> 
//      Athul.M.A
// </author>
//
// <date> 
//      15-11-2018 
// </date>
// 
// <reviewer>
//      Ayush Mittal
// </reviewer>
//
// <copyright file="DataStatusNotifierTest.cs" company="B'15, IIT Palakkad">
//      This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
//
// <summary>
//      This file contains Tests for Data Status Notifier Component 
//      This file is a part of Networking Module Unit Testing
// </summary>
// -----------------------------------------------------------------------

namespace Masti.Networking.UnitTests
{
    using System.Collections.Generic;
    using Masti.QualityAssurance;
    using Masti.Schema;

    /// <summary>
    /// Unit Test for Data Status Notifier Component.
    /// </summary>
    public class DataStatusNotifierTest : ITest
    {
        /// <summary>
        /// Holds communication Instance
        /// </summary>
        private Communication communication;

        /// <summary>
        /// status to check that ImageSharing module is notified.  
        /// </summary>
        private bool imageModuleNotified = false;

        /// <summary>
        /// status to check that Messaging module is notified. 
        /// </summary>
        private bool messageModuleNotified = false;

        /// <summary>
        /// ImageSchema object to encode data
        /// </summary>
        private ISchema imageSchema;

        /// <summary>
        /// MessageSchema object to encode data
        /// </summary>
        private ISchema messageSchema;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataStatusNotifierTest" /> class.
        /// </summary>
        /// <param name="logger">Logger object used for logging during test</param>
        public DataStatusNotifierTest(ILogger logger)
        {
            this.Logger = logger;
        }

        /// <summary>
        /// Gets or sets Logger instance.
        /// </summary>
        public ILogger Logger { get; set; }

        /// <summary>
        /// Function for running the unit tests
        /// </summary>
        /// <returns>true on success and false on failure</returns>
        public bool Run()
        {
            // setting communication to Communication class instance. 
            this.communication = new Communication(8030, 3, 1);

            // ImageSchema object for this test.
            this.imageSchema = new ImageSchema();

            // MessageSchema object for this test
            this.messageSchema = new MessageSchema();

            // Run test for subscription
            bool subscribeTeststatus = this.SubscribeForDataStatusTest();

            // Run test for notify function
            bool dataStatusNotifyTestStatus = this.DataStatusNotifyTest();

            if (this.communication != null)
            {
                this.communication.Dispose();
            }

            return subscribeTeststatus && dataStatusNotifyTestStatus; 
        }

        /// <summary>
        /// Sample event handler for ImageSharing module that logs the status code
        /// </summary>
        /// <param name="data">The whole message data</param>
        /// <param name="status">Status code as received from the calling module</param>
        public void HandleImageStatusNotification(string data, StatusCode status)
        {
            this.imageModuleNotified = true;
            this.Logger.LogSuccess("ImageSharing: Status code returned: " + status);
        }

        /// <summary>
        /// Sample event handler for Message module that logs the status code
        /// </summary>
        /// <param name="data">The whole message data</param>
        /// <param name="status">Status code as received from the calling module</param>
        public void HandleMessageStatusNotification(string data, StatusCode status)
        {
            this.messageModuleNotified = true;
            this.Logger.LogSuccess("Messaging: Status code returned: " + status);
        }

        /// <summary>
        /// Function to test the SubscribeForDataStatus function in Communication module
        /// </summary>
        /// /// <returns>true on successful completion of the SubscribeForDataStatus test; false otherwise</returns>
        public bool SubscribeForDataStatusTest()
        {
            bool subscribeStatus = false;
            subscribeStatus = this.communication.SubscribeForDataStatus(DataType.ImageSharing, this.HandleImageStatusNotification);
            if (subscribeStatus)
            {
                this.Logger.LogSuccess("Subscription for Data Status for " + DataType.ImageSharing.ToString() + " completed successfully.");
            }
            else
            {
                this.Logger.LogError("Subscription for Data Status for " + DataType.ImageSharing.ToString() + " failed.");
            }

            if (subscribeStatus == false)
            {
                return subscribeStatus;
            }

            subscribeStatus = false;
            subscribeStatus = this.communication.SubscribeForDataStatus(DataType.Message, this.HandleMessageStatusNotification);
            if (subscribeStatus)
            {
                this.Logger.LogSuccess("Subscription for Data Status for " + DataType.Message.ToString() + " completed successfully.");
            }
            else
            {
                this.Logger.LogError("Subscription for Data Status for " + DataType.Message.ToString() + " failed.");
            }

            return subscribeStatus;
        }

        /// <summary>
        /// Function to test the DataStatusNotify function of the Communication module
        /// </summary>
        /// <returns>true on successful completion of the DataStatusNotify test; false otherwise</returns>
        public bool DataStatusNotifyTest()
        {
            Dictionary<string, string> tagDict = new Dictionary<string, string>();
            tagDict.Add("data", "TestData");

            string encodeResult = this.imageSchema.Encode(tagDict);
            this.communication.DataStatusNotify(encodeResult, StatusCode.Success);
            if (!this.imageModuleNotified)
            {
                return false;
            }

            encodeResult = this.messageSchema.Encode(tagDict);
            this.communication.DataStatusNotify(encodeResult, StatusCode.Success);
            if (!this.messageModuleNotified)
            {
                return false;
            }
            else
            {
                return true;
            }            
        }
    }
}